console.log('FUNCTIONS LIST:');
console.log('addCourse(id, name, description, price, isActive);');
console.log('archiveCourse(nameOfCourse); //use name of course as argument');
console.log('deleteCourse(); //delete last course in array');
console.log('getSingleCourse(id)// get single course by course ID');
console.log('getAllCourses();// display current courses array');
console.log('showActive(); // display all active courses/isActive == true')

let courses = [

	{
		id:"1",
		name:"Computer Science",
		description: "asdasdasd",
		price: 7000,
		isActive: true
	},

	{
		id:"2",
		name:"Hotel & Restaurant Management",
		description: "feeasdasd",
		price: 6000,
		isActive: true
	},

	{
		id:"3",
		name:"Criminology",
		description: "asdds",
		price: 4000,
		isActive: true
	},

	{
		id:"4",
		name:"Business Administration",
		description: "asdad",
		price: 9000,
		isActive: false
	}

];


//add
const addCourse = (id,name,description,price,isActive) => {

		let add = {

			id:id,
			name:name,
			description:description,
			price: price,
			isActive: isActive
		};


		if(courses.push(add)){

		alert(`You have created ${name}. The Price is ${price}`)
		}else{

			alert('Error Adding');
		}

}


//update isActive to false
//const archiveCourse = (index) => courses[index].isActive = false;

//delete last course in the array
const deleteCourse = () => {

		courses.pop();
		console.log(`Last course in array deleted. Updated Array below.`);
		console.log(courses);

}

//get single course
 const getSingleCourse = id => courses.find(course => course.id == id);


 //get all courses()
 const getAllCourses = () => courses;

 //Stretch Goal///show active courses only
 const showActive = () => courses.filter(course => course.isActive == true);


 //Stretch Goal /// archiveCourse by name
 const archiveCourse = name =>{

 	let index = courses.findIndex(course => course.name == name);

 	//update the value to false

 	if(index != -1){

 		courses[index].isActive = false;
 		console.log(`Successfully archived this course`);
 		console.log(courses[index]);

 	}else{

 		alert('Not found');
 	}
 	

 } 
